+++
+++
<a class="anchor" id="about"></a>
<section class="pt50 pb50">
    <div class="container">
        <div class="section_title">
            <h3 class="title">
                What is FlowCon France ?
            </h3>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-12">
                <p style="text-align: justify;">
                    After 6 editions that have placed it among the reference conferences in France, Lean Kanban France has became FlowCon France.
                    We wanted a new identity that is more in line with what we want to share and promote : the <b>flow</b>. 
                    Our goal is to help all those who want to make their organizations more fluid.
                    FlowCon is about offering quality content, French and international, in a warm and informal setting around topics such as Kanban, agility, continuous delivery, user experience, decentralized management, etc.
                </p>
                <p>
                    We have also the pleasure to invite Don Reinertsen, author of the seminal book about principles of product development flow, to teach his masterclass. <a href="/flow-masterclass/">More information here</a>
                </p>
                <p>
                    FlowCon has also an associated meetup with regular events: 
                    <a target="_blank" href="https://www.meetup.com/fr-FR/FlowCon-France-Meetup/">https://www.meetup.com/fr-FR/FlowCon-France-Meetup/</a>
                </p>
            </div>
        </div>
        <div class="row justify-content-center mt30">
            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="fa fa-diamond"></i>
                    <div class="content">
                        <h4>Our value</h4>
                        <p>
                            A deep dive into software product development flow
                        </p>
                        <!--a href="#">more</a-->
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="fa fa-plane"></i>
                    <div class="content">
                        <h4>International</h4>
                        <p>
                            Speakers are coming from all around the world
                        </p>
                        <!--a href="#">more</a-->
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="fa fa-hourglass"></i>
                    <div class="content">
                        <h4>2 days</h4>
                        <p>
                            2 days of conferences in a large venue
                        </p>
                        <!--a href="#">more</a-->
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="icon_box_one">
                    <i class="fa fa-th"></i>
                    <div class="content">
                        <h4>5 tracks</h4>
                        <p>
                            5 tracks with 30+ talks and workshops
                        </p>
                        <!--a href="#">more</a-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
